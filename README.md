﻿
# The End Project
*Licensed under CC BY-NC-SA 4.0*

This video is my project for a course in 3D modelling and animation in an open source environment.
The whole project is available on Gitlab: https://gitlab.com/woodbase/endproject

## How to render
The end video consists of several image sequences, to make sure everything is rendered correctly open and render these files from the "blenderfiles" folder:

 - Tunnel/tunnel_path16.blend
 - Hallway/finalFulllength.blend
 - closing credits/closing_credits.blend


That should make it possible to render out the final video clip from:
***video/video4.blend***

  

## Credits

**Sound design**
Zombie by gneube (https://freesound.org/s/315847/) CC BY 3.0
Leaves crunching by PaulOCone (https://freesound.org/s/460236/) CC0 1.0
Ambience, Night Wildlife, A by InspectorJ (https://freesound.org/s/352514/) CC BY 3.0
Wet thump or splash by 150128 (https://freesound.org/s/326658/) CC BY-NC 3.0
Explosion by Iwiploppenisse (https://freesound.org/s/156031/) CC BY 3.0

**Graphics**
https://commons.wikimedia.org/wiki/File:Zombie_Worlds_FINAL_39x.png
Photo by Wendelin Jacober from Pexels
Photo by Brett Sayles from Pexels
Photo by Valeria Boltneva from Pexels
Photo by Blank Space from Pexels

**Animation design**
Ramar Digital

**3D Modelling and design**
jlnh (https://www.blendswap.com/blend/16216) CC BY
Mixamo.com CC BY-NC-SA 3.0
Ramar Digital

**Special thanks to**
My dear Ramona for her patience
Niklas Folkegård
Högskolan i Gävle
Blender Foundation
